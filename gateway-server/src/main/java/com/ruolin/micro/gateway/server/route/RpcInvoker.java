package com.ruolin.micro.gateway.server.route;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@FunctionalInterface
public interface RpcInvoker {
    void doInvoke(HttpServletRequest request, HttpServletResponse response);
}
