package com.ruolin.micro.gateway.server.bean;

import com.ruolin.micro.gateway.server.metadata.InstanceMetadata;
import org.apache.dubbo.config.spring.ReferenceBean;
import org.apache.dubbo.rpc.service.GenericService;

public class RouteContext {
    private String route;
    private ReferenceBean<GenericService> referenceBean;
    private InstanceMetadata instanceMetadata;

    public ReferenceBean<GenericService> getReferenceBean() {
        return referenceBean;
    }

    public void setReferenceBean(ReferenceBean<GenericService> referenceBean) {
        this.referenceBean = referenceBean;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public InstanceMetadata getInstanceMetadata() {
        return instanceMetadata;
    }

    public void setInstanceMetadata(InstanceMetadata instanceMetadata) {
        this.instanceMetadata = instanceMetadata;
    }
}
