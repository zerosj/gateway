package com.ruolin.micro.gateway.server.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.ruolin.micro.gateway.server.metadata.ApplicationRouteMetadata;
import com.ruolin.micro.gateway.server.metadata.MethodRouteMetadata;
import org.apache.dubbo.config.MetadataReportConfig;

import java.util.List;
import java.util.Properties;

public class MetadataCenterServiceTest {
    public static void main(String[] args) {
        MetadataReportConfig mrc = new MetadataReportConfig("nacos://218.78.55.64:8848");
        try {
            String serverAddr = "{serverAddr}";
            String dataId = "com.github.dubbo.example.api.EchoService:1.0:myGroup:provider:dubbo-example-provider";
            String group = "dubbo";
            Properties properties = new Properties();
            properties.put("serverAddr", "http://218.78.55.64:8848");
            ConfigService configService = NacosFactory.createConfigService(properties);
            String content = configService.getConfig(dataId, group, 5000);
            ApplicationRouteMetadata metadata = JSON.parseObject(content, ApplicationRouteMetadata.class, Feature.values());
            MethodRouteMetadata methodRouteMetadata = metadata.getMethods().get(1);
            List<String> parameterTypes = methodRouteMetadata.getParameterTypes();
            String[] parameterArray = parameterTypes.toArray(new String[parameterTypes.size()]);
            System.out.println(content);
        } catch (NacosException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
