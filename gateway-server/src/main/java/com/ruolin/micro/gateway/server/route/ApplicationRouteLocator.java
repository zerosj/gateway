package com.ruolin.micro.gateway.server.route;

import com.ruolin.micro.gateway.server.servlet.bean.RouterBeanDefinition;
import com.ruolin.micro.gateway.server.metadata.ApplicationRouteMetadata;
import com.ruolin.micro.gateway.server.metadata.MethodRouteMetadata;

import java.util.List;

public interface ApplicationRouteLocator {
    MethodRouteMetadata getMethodRouteMetadata(String requestRoute, List<RouterBeanDefinition> routerBeanDefinitions);

    ApplicationRouteMetadata getApplicationRouteMetadata(String requestRoute, List<RouterBeanDefinition> routerBeanDefinitions);
}
