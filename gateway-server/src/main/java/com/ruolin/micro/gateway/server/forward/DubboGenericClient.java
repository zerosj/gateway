package com.ruolin.micro.gateway.server.forward;


import com.ruolin.micro.gateway.server.bean.RequestParamResolverHolder;
import com.ruolin.micro.gateway.server.cache.ApplicationGenericServiceCache;
import com.ruolin.micro.gateway.server.constant.FormType;
import com.ruolin.micro.gateway.server.constant.RequestMethod;
import com.ruolin.micro.gateway.server.constant.RouterConstant;
import com.ruolin.micro.gateway.server.servlet.bean.RouterBeanDefinition;
import com.ruolin.micro.gateway.server.metadata.MethodRouteMetadata;
import com.ruolin.micro.gateway.server.route.resolver.RequestParameterResolver;
import com.ruolin.micro.gateway.server.route.DefaultApplicationRouteLocator;
import com.ruolin.micro.gateway.server.route.RouteDefinitionLocator;
import com.ruolin.micro.gateway.server.utils.ResponseUtils;
import org.apache.dubbo.config.spring.ReferenceBean;
import org.apache.dubbo.rpc.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CompletableFuture;

public class DubboGenericClient extends DefaultApplicationRouteLocator {
    @Autowired
    private RequestParameterResolver requestBodyParameterResolver;

    @Autowired
    private RequestParameterResolver requestFormParameterResolver;

    @Autowired
    private RequestParameterResolver pathVariableParameterResolver;

    @Autowired
    private RouteDefinitionLocator routeDefinitionLocator;

    public void $invoke(HttpServletRequest request, HttpServletResponse response) {

         HttpProcessorFunction.process(request, response);
//        String requestMethod = request.getMethod();
//
//        if (requestMethod.equals(RequestMethod.GET.name())) {
//            doGet(request, response);
//
//        } else if (requestMethod.equals(RequestMethod.POST.name())) {
//            doPost(request, response);
//
//        } else if (requestMethod.equals(RequestMethod.DELETE.name())) {
//            doDelete(request, response);
//        }
    }

//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
//        Object[] args = requestBodyParameterResolver.resolve(req, resp);
//        CompletableFuture.runAsync(() -> this.doInvoke(req, resp, args));
//
//    }
//
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
//        Object[] args = requestFormParameterResolver.resolve(req, resp);
//        CompletableFuture.runAsync(() -> this.doInvoke(req, resp, args));
//
//    }
//
//    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
//        Object[] args = pathVariableParameterResolver.resolve(req, resp);
//        this.doInvoke(req, resp, args);
//    }
//
//    private void doInvoke(HttpServletRequest request, HttpServletResponse response, Object[] args) {
//        MethodRouteMetadata methodRouteMetadata = getMethodRouteMetadata(request.getPathInfo(), routeDefinitionLocator.getRouteDefinitions());
//        RouterBeanDefinition routerBeanDefinition = getRouterBeanDefinition(request.getPathInfo(), routeDefinitionLocator.getRouteDefinitions());
//        String application = routerBeanDefinition.getUri().substring(RouterConstant.SYMBOL.length());
//        ReferenceBean<GenericService> serviceReferenceBean = ApplicationGenericServiceCache.getCache(application);
//        GenericService genericService = serviceReferenceBean.get();
//        Object hello = genericService.$invoke(methodRouteMetadata.getName(), methodRouteMetadata.getParameterTypeArray(), args);
//        ResponseUtils.returnJson(response, hello);
//        response.setStatus(HttpStatus.OK.value());
//    }

}
