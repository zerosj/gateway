package com.ruolin.micro.gateway.server.constant;

public class PropertiesConstant {
    public static final String REGISTRY = "micro.gateway.registry-address";
    public static final String  NACOS_SERVER_ADDR = "micro.gateway.nacos.discovery.server-address";
    public static final String CLUSTER = "micro.gateway.com.ruolin.micro.gateway.server.cluster";
    public static final String ENTITY_PACKAGE = "micro.gateway.entity";
}
