package com.ruolin.micro.gateway.server.servlet;


import com.ruolin.micro.gateway.server.forward.DubboGenericClient;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@WebServlet(urlPatterns = "/d/*")
public class DubboGateway extends HttpServlet {
    @Autowired
    private DubboGenericClient dubboGenericClient;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
        CompletableFuture.runAsync(() ->  dubboGenericClient.$invoke(request, response)).join();

    }
}
