package com.ruolin.micro.gateway.server.utils;


import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Pattern;

public class DataTypePatternUtils {

    private static final String INTEGER_PATTERN = "^[0-9]*$";
    private static final String INTEGER_CLASS = Integer.class.getName();
    private static final String LONG_CLASS = Long.class.getName();
    private static final String FLOAT_CLASS = Float.class.getName();
    private static final String DOUBLE_CLASS = Double.class.getName();
    private static final String DECIMAL_CLASS = BigDecimal.class.getName();
    private static final String DATE_CLASS = Date.class.getName();
    private static final String BOOLEAN_CLASS = Boolean.class.getName();
    private static final String DECIMAL_PATTERN = "(-?\\d+)(\\.\\d+)?$";
    private static final String DATE_PATTERN = "^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$";
    private static final String DATE_PATTERN2 = "^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\\\\s+([0-1]?[0-9]|2[0-3])-([0-5][0-9])-([0-5][0-9])$";

    private DataTypePatternUtils() {
    }

    public static void main(String[] args) {
        boolean matches = Pattern.matches("^[-\\\\+]?[\\\\d]*$", "1");
        System.out.println(dataTypeMatch(DATE_CLASS, "2021-02-04 14:07:55"));
        System.out.println(matches);

    }

    public static boolean dataTypeMatch(String type, Object arg) {
        if (INTEGER_CLASS.equals(type) || LONG_CLASS.equals(type)) {
            return Pattern.matches(INTEGER_PATTERN, (CharSequence) arg);
        } else if (FLOAT_CLASS.equals(type) || DOUBLE_CLASS.equals(type) || DECIMAL_CLASS.equals(type)) {
            return Pattern.matches(DECIMAL_PATTERN, (CharSequence) arg);
        } else if (BOOLEAN_CLASS.equals(type)) {
            return Boolean.TRUE.toString().equals(arg) || Boolean.FALSE.toString().equals(arg);
        }
        return false;
    }
}
