package com.ruolin.micro.gateway.server.discovery;

import org.apache.dubbo.registry.client.ServiceInstance;

public abstract class AbstractServiceDiscoveryAdapter implements ServiceDiscoveryAdapter {

    @Override
    public void destroy() throws Exception {
    }

    @Override
    public void register(ServiceInstance serviceInstance) throws RuntimeException {

    }

    @Override
    public void update(ServiceInstance serviceInstance) throws RuntimeException {

    }

    @Override
    public void unregister(ServiceInstance serviceInstance) throws RuntimeException {

    }
}
