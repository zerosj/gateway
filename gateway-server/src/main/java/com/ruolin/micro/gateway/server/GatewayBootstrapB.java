package com.ruolin.micro.gateway.server;

import com.ruolin.micro.gateway.server.constant.PropertiesConstant;
import com.ruolin.micro.gateway.server.discovery.NacosServiceDiscoveryAdapter;
import com.ruolin.micro.gateway.server.discovery.ServiceDiscoveryAdapter;
import org.apache.dubbo.common.URL;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;

@ServletComponentScan(value = "com.ruolin.micro.gateway.server.servlet")
@SpringBootApplication
public class GatewayBootstrapB {
    public static void main(String[] args) {
       // System.setProperty(PropertiesConstant.ENTITY_PACKAGE,"com.ruolin.micro.gateway.server.entity");
        System.setProperty("spring.profiles.active","b");
        SpringApplication.run(GatewayBootstrapB.class, args);
    }

    @Bean
    public ServiceDiscoveryAdapter nacosServiceDiscovery() {
        return new NacosServiceDiscoveryAdapter();
    }
}
