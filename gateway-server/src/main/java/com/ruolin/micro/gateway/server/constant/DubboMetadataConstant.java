package com.ruolin.micro.gateway.server.constant;

public class DubboMetadataConstant {
    public static final String SIDE = "side";
    public static final String METHODS = "methods";
    public static final String RELEASE = "release";
    public static final String DEPRECATED = "deprecated";
    public static final String DBUBO = "dubbo";
    public static final String INTERFACE = "interface";
    public static final String VERSION = "version";
    public static final String REVERSION = "revision";
    public static final String GENERIC = "generic";
    public static final String DEFAULT = "default";
    public static final String PROTOCOL = "protocol";
    public static final String APPLICATION = "application";
    public static final String DYNAMIC = "dynamic";
    public static final String CATEGORY = "category";
    public static final String ANYHOIST = "anyhost";
    public static final String GROUT = "group";
    public static final String TIMESTAMP = "timestamp";
    public static final String GENERIC_VALUE = "true";
    public static final String METADATA_GROUP = "dubbo";
    public static final String ROUTER_GROUP = "route";
}
