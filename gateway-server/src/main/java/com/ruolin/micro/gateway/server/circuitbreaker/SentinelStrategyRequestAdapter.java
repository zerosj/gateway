package com.ruolin.micro.gateway.server.circuitbreaker;

import javax.servlet.http.HttpServletRequest;

public interface SentinelStrategyRequestAdapter {
    String parseOrigin(HttpServletRequest request);
}
