package com.ruolin.micro.gateway.server.discovery;


import org.apache.dubbo.common.utils.Page;
import org.apache.dubbo.registry.client.ServiceDiscovery;
import org.apache.dubbo.registry.client.ServiceInstance;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * application service discovery adapter
 */

public interface ServiceDiscoveryAdapter extends ServiceDiscovery {

}
