package com.ruolin.micro.gateway.server.cache;

import com.google.common.cache.AbstractCache;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.ruolin.micro.gateway.server.metadata.MethodRouteMetadata;

import javax.annotation.CheckForNull;

/**
 * route and method's metadata  mapping
 */

public class MethodRouteMetadataCache extends AbstractCache<String, MethodRouteMetadata> {
    private static final Cache<String, MethodRouteMetadata> METHOD_METADATA_CACHE = CacheBuilder.newBuilder().build();

    private MethodRouteMetadataCache() {
    }

    @CheckForNull
    @Override
    public MethodRouteMetadata getIfPresent(Object key) {
        return METHOD_METADATA_CACHE.getIfPresent(key);
    }

    public static Cache<String, MethodRouteMetadata> getMethodMetadataCache() {
        return METHOD_METADATA_CACHE;
    }
}
