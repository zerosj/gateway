package com.ruolin.micro.gateway.server.metadata;


import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author zhaoshengjie
 */

public class Type implements Serializable {
    private String type;
    private Map<String, Object> properties = new LinkedHashMap<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Object> properties) {
        this.properties = properties;
    }
}
