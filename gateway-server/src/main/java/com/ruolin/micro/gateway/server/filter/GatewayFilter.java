package com.ruolin.micro.gateway.server.filter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

public abstract class GatewayFilter implements Filter {
    protected String getRoute(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        return StringUtils.remove(requestURI, request.getServletPath());
    }

    abstract int getIndex();
}
