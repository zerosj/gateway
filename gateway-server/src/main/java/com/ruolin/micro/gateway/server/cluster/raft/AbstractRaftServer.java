package com.ruolin.micro.gateway.server.cluster.raft;

import cn.hutool.core.collection.ListUtil;
import com.alipay.sofa.jraft.JRaftUtils;
import com.alipay.sofa.jraft.conf.Configuration;
import com.alipay.sofa.jraft.entity.PeerId;
import com.google.common.collect.Iterables;
import com.ruolin.micro.gateway.server.constant.ClusterProertiesConstant;
import org.assertj.core.util.IterableUtil;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AbstractRaftServer implements RaftServer, EnvironmentAware {
    private Environment environment;
    private final String SYMBOL = ":";

    @Override
    public void start() {
        String endpoints = environment.getProperty(ClusterProertiesConstant.ENDPOINTS);
        Configuration configuration = JRaftUtils.getConfiguration(endpoints);



    }

    @Override
    public boolean run() {
        return false;
    }

    @Override
    public void startAfter() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public static void main(String[] args) {
        PeerId peerId = PeerId.parsePeer("127.0.0.1:9001");
        System.out.println(peerId.getIp());
    }
}
