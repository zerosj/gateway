package com.ruolin.micro.gateway.server.constant;

public enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE
}
