package com.ruolin.micro.gateway.server.event;

import com.ruolin.micro.gateway.server.bean.RouteContext;
import org.springframework.context.ApplicationEvent;

public class RouteContextRefreshEvent extends ApplicationEvent {
    private RouteContext routeContext;
    public RouteContextRefreshEvent(RouteContext routeContext) {
        super(routeContext);
        this.routeContext = routeContext;
    }

    public RouteContext getRouteContext() {
        return routeContext;
    }

    public void setRouteContext(RouteContext routeContext) {
        this.routeContext = routeContext;
    }
}
