package com.ruolin.micro.gateway.server.circuitbreaker;

import javax.servlet.http.HttpServletRequest;

public class DefaultSentinelStrategyRequestAdapter implements SentinelStrategyRequestAdapter{
    @Override
    public String parseOrigin(HttpServletRequest request) {
        return null;
    }
}
