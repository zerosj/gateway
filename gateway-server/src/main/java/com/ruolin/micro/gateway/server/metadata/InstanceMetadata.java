package com.ruolin.micro.gateway.server.metadata;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ruolin.micro.gateway.server.constant.DubboMetadataConstant;

import java.io.Serializable;
import java.util.Map;

public class InstanceMetadata implements Serializable {
    private String side;
    private String release;
    private String methods;
    private String deprecated;
    private String revision;
    @JsonProperty(value = "metadata-type")
    @JSONField(name = "metadata-type")
    private String metadataType;
    private String dynamic;
    private String anyhost;
    private String group;
    private String version;
    private String generic;
    private String application;
    private String address;
    @JsonProperty(value = "interface")
    @JSONField(name = "interface")
    private String interfaceName;
    private String protocol = "dubbo";
    private String cluster = "failover";

    public InstanceMetadata convert(Map<String, String> metadata) {
        this.setInterfaceName(metadata.get(DubboMetadataConstant.INTERFACE));
        this.setVersion(metadata.get(DubboMetadataConstant.VERSION));
        this.setGeneric(metadata.get(DubboMetadataConstant.GENERIC));
        this.setApplication(metadata.get(DubboMetadataConstant.APPLICATION));
        this.setProtocol(metadata.get(DubboMetadataConstant.PROTOCOL));
        this.setGroup(metadata.get(DubboMetadataConstant.GROUT));
        return this;
    }


    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getMethods() {
        return methods;
    }

    public void setMethods(String methods) {
        this.methods = methods;
    }

    public String getDeprecated() {
        return deprecated;
    }

    public void setDeprecated(String deprecated) {
        this.deprecated = deprecated;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(String metadataType) {
        this.metadataType = metadataType;
    }

    public String getDynamic() {
        return dynamic;
    }

    public void setDynamic(String dynamic) {
        this.dynamic = dynamic;
    }

    public String getAnyhost() {
        return anyhost;
    }

    public void setAnyhost(String anyhost) {
        this.anyhost = anyhost;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGeneric() {
        return generic;
    }

    public void setGeneric(String generic) {
        this.generic = generic;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }
}
