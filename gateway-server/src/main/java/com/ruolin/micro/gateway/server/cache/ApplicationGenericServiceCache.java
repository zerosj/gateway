package com.ruolin.micro.gateway.server.cache;

import com.google.common.cache.AbstractCache;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.dubbo.config.spring.ReferenceBean;
import org.apache.dubbo.rpc.service.GenericService;

import javax.annotation.CheckForNull;

public class ApplicationGenericServiceCache extends AbstractCache<String, ReferenceBean<GenericService>> {
    private static final Cache<String, ReferenceBean<GenericService>> GENERIC_SERVICE_CACHE = CacheBuilder.newBuilder().build();

    private ApplicationGenericServiceCache() {
    }

    @CheckForNull
    @Override
    public ReferenceBean<GenericService> getIfPresent(Object key) {
        return GENERIC_SERVICE_CACHE.getIfPresent(key);
    }

    public static Cache<String, ReferenceBean<GenericService>> getCache() {
        return GENERIC_SERVICE_CACHE;
    }

    public static ReferenceBean<GenericService> getCache(String application){
        return GENERIC_SERVICE_CACHE.getIfPresent(application);
    }
}
