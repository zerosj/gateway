package com.ruolin.micro.gateway.server.route.resolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * http request message's parameter resolver
 *
 * @author zhaoshengjie
 */
public interface RequestParameterResolver {
    Object[] resolve(HttpServletRequest request, HttpServletResponse response);
}
