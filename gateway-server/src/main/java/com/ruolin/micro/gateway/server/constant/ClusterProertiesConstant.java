package com.ruolin.micro.gateway.server.constant;

public class ClusterProertiesConstant {
    private ClusterProertiesConstant(){}

    public static final String ENDPOINTS = "gateway.cluster.endpoints";
}
