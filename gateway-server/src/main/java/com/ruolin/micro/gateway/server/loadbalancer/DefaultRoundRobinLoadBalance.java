package com.ruolin.micro.gateway.server.loadbalancer;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.RpcException;
import org.apache.dubbo.rpc.cluster.loadbalance.RoundRobinLoadBalance;

import java.util.List;

public class DefaultRoundRobinLoadBalance extends RoundRobinLoadBalance {

    @Override
    public <T> Invoker<T> doSelect(List<Invoker<T>> invokers, URL url, Invocation invocation) throws RpcException {
        return super.doSelect(invokers, url, invocation);
    }
}
