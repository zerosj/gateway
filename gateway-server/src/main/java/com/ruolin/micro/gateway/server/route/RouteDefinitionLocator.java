package com.ruolin.micro.gateway.server.route;

import com.ruolin.micro.gateway.server.servlet.bean.RouterBeanDefinition;

import java.util.List;

public interface RouteDefinitionLocator {
    List<RouterBeanDefinition> getRouteDefinitions();
}
