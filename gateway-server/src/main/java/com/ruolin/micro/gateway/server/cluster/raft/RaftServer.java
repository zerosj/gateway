package com.ruolin.micro.gateway.server.cluster.raft;

public interface RaftServer {
    void start();

    boolean run();

    void startAfter();

    void destroy();
}
