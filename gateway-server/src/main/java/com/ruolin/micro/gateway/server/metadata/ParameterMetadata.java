package com.ruolin.micro.gateway.server.metadata;


import java.io.Serializable;

/**
 * Parameter metadata
 */

public class ParameterMetadata implements Serializable {
    private String name;
    private Object value;
    private int index;

    public ParameterMetadata() {
    }

    public ParameterMetadata(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public ParameterMetadata(String name, Object value, int index) {
        this.name = name;
        this.value = value;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
