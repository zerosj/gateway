package com.ruolin.micro.gateway.server.cache;

import com.google.common.cache.AbstractCache;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.ruolin.micro.gateway.server.metadata.ApplicationRouteMetadata;

import javax.annotation.CheckForNull;

/**
 * dubbo metadata cache
 */

public class ApplicationRouterMetadataCache extends AbstractCache<String, ApplicationRouteMetadata> {
    private static final Cache<String, ApplicationRouteMetadata> METADATA_CACHE = CacheBuilder.newBuilder().build();

    private ApplicationRouterMetadataCache() {
    }

    public static Cache<String, ApplicationRouteMetadata> getMetadataCache() {
        return METADATA_CACHE;
    }

    @CheckForNull
    @Override
    public ApplicationRouteMetadata getIfPresent(Object key) {
        return METADATA_CACHE.getIfPresent(key);
    }
}
