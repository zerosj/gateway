package com.ruolin.micro.gateway.server.filter;


import javax.servlet.*;
import javax.servlet.FilterChain;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

//@Component
@WebFilter(filterName = "gatewayFilter0", urlPatterns = "/d/*")
public class GatewayRouteFilter extends GatewayFilter {

    @Override
    public int getIndex() {
        return 0;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        if (servletRequest instanceof HttpServletRequest){
//            HttpServletRequest request = (HttpServletRequest) servletRequest;
//            String requestURI = request.getRequestURI();
//            String route = StringUtils.remove(requestURI, request.getServletPath());
//            Collection<String> routes = ServiceRouteCache.getRoutes();
//            if(routes.contains(route)){
//                log.info("The access route is correct,route ::{}",route);
//            }
//            else {
//                log.info("The access route is not exist");
//            }
//        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
