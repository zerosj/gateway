package com.ruolin.micro.gateway.server.bean;

import com.ruolin.micro.gateway.server.constant.FormType;
import com.ruolin.micro.gateway.server.route.resolver.AbstractRequestParameterResolver;
import org.springframework.util.Assert;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class RequestParamResolverHolder {

    private static RequestParamResolverHolder INSTANCE  = new RequestParamResolverHolder();

    private volatile ConcurrentMap<FormType, AbstractRequestParameterResolver> resolverMap = new ConcurrentHashMap<>(1 << 2);

    private RequestParamResolverHolder(){}

    public final static RequestParamResolverHolder getResolverHolder(){
        return INSTANCE;
    }

    public final void put(FormType type, AbstractRequestParameterResolver resolver){
        this.resolverMap.put(type, resolver);
    }


    public final ConcurrentMap<FormType, AbstractRequestParameterResolver> getResolverMap() {
        return resolverMap;
    }

    public final AbstractRequestParameterResolver getResolver(FormType type){
        Assert.notEmpty(this.resolverMap, "resolverMap is null");

        return this.resolverMap.get(type);
    }

}
