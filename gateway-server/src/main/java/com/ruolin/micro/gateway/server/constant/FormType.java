package com.ruolin.micro.gateway.server.constant;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * request data type
 */
public enum FormType {
    GET("GET", "FORM"),
    PATH("PATH", "PATHVARIABLE"),
    POST("POST","BODY"),
    PUT("PUT", "BODY"),

    DELETE("DELETE", "FORM");

    private String code;
    private String type;

   FormType(String code, String type){
     this.code = code;
     this.type = type;
   }

    public static FormType  getRequestMethod(String code){
        return Stream.of(FormType.values())
                .collect(Collectors.toMap(FormType::getCode, Function.identity()))
                .get(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
