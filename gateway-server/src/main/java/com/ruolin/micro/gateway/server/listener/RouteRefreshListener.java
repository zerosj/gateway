package com.ruolin.micro.gateway.server.listener;

import com.ruolin.micro.gateway.server.bean.RouteContext;
import com.ruolin.micro.gateway.server.cache.RouteReferenceBeanMappingCache;
import com.ruolin.micro.gateway.server.event.RouteContextRefreshEvent;
import com.ruolin.micro.gateway.server.utils.GatewayUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class RouteRefreshListener implements ApplicationListener<RouteContextRefreshEvent> {
    @Override
    public void onApplicationEvent(RouteContextRefreshEvent event) {
        RouteContext routeContext = event.getRouteContext();
        String route = routeContext.getRoute();

        RouteReferenceBeanMappingCache.getBeanMapping().put(route, GatewayUtils.buildReferenceBean(routeContext.getInstanceMetadata()));
    }
}
