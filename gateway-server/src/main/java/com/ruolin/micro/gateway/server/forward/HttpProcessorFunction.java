package com.ruolin.micro.gateway.server.forward;

import com.ruolin.micro.gateway.server.bean.RequestParamResolverHolder;
import com.ruolin.micro.gateway.server.constant.FormType;
import com.ruolin.micro.gateway.server.route.resolver.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.CompletableFuture;

public final class HttpProcessorFunction {
   private HttpProcessorFunction(){}

   public static void process(HttpServletRequest request, HttpServletResponse response){
       FormType formType = FormType.getRequestMethod(request.getMethod());
       AbstractRequestParameterResolver resolver = RequestParamResolverHolder.getResolverHolder().getResolver(formType);
       resolver.doInvoke(request, response);
      // CompletableFuture.runAsync(() ->  resolver.doInvoke(request, response));
   }

}
