package com.ruolin.micro.gateway.server.servlet;


import java.util.List;

public class GatewayRouteMetadata {
    private String id;
    private List<GatewayPredicateMetadata> predicateMetadata;
    private List<GatewayFilterMetadata> gatewayFilterMetadata;
    private String uri;
    private int order = 0;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<GatewayPredicateMetadata> getPredicateMetadata() {
        return predicateMetadata;
    }

    public void setPredicateMetadata(List<GatewayPredicateMetadata> predicateMetadata) {
        this.predicateMetadata = predicateMetadata;
    }

    public List<GatewayFilterMetadata> getGatewayFilterMetadata() {
        return gatewayFilterMetadata;
    }

    public void setGatewayFilterMetadata(List<GatewayFilterMetadata> gatewayFilterMetadata) {
        this.gatewayFilterMetadata = gatewayFilterMetadata;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
