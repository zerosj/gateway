package com.ruolin.micro.gateway.server.config;

import com.ruolin.micro.gateway.server.servlet.bean.RouterBeanDefinition;
import com.ruolin.micro.gateway.server.route.RouteDefinitionLocator;

import java.util.List;

public class PropertiesRouteDefinitionLocator  implements RouteDefinitionLocator {
    private final MicroGatewayProperties properties;

    public PropertiesRouteDefinitionLocator(final MicroGatewayProperties properties){
        this.properties = properties;
    }

    @Override
    public List<RouterBeanDefinition> getRouteDefinitions() {
        return properties.getRoutes();
    }
}
