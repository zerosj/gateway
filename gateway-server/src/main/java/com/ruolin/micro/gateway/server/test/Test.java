package com.ruolin.micro.gateway.server.test;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.ruolin.micro.gateway.server.utils.DubboServiceUtils;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.registry.client.ServiceInstance;
import org.apache.dubbo.registry.nacos.NacosServiceDiscovery;

import java.util.List;
import java.util.Set;


public class Test {

    public static void main(String[] args) throws Exception {
        try {
            //http://192.168.22.252:18848?nacos.group=ljhy-rel
            NacosServiceDiscovery discovery = new NacosServiceDiscovery();
//            discovery.initialize(URL.valueOf("http://218.78.55.64:8848"));
            discovery.initialize(URL.valueOf("http://218.78.55.64:8848"));
            Set<String> services = discovery.getServices();
            Set<String> strings = DubboServiceUtils.filterDubboService(services);
            NamingService namingService = NamingFactory.createNamingService("http://218.78.55.64:8848");
            List<Instance> allInstances = namingService.getAllInstances("providers:com.github.dubbo.example.api.EchoService:1.0:");
            List<ServiceInstance> instances = discovery.getInstances("providers:com.github.dubbo.example.api.EchoService:1.0:");
            System.out.println(strings);
//            List<ServiceInstance> instances = discovery.getInstances("providers:com.github.dubbo.example.api.EchoService:1.0:");
//            NamingService namingService = NamingFactory.createNamingService("http://218.78.55.64:8848");
//            List<Instance> allInstances = namingService.getAllInstances("providers:com.github.dubbo.example.api.EchoService:1.0:");
        } catch (NacosException e) {
            e.printStackTrace();
        }
    }
}
