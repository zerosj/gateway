package com.ruolin.micro.gateway.server.config;

import com.ruolin.micro.gateway.server.servlet.bean.RouterBeanDefinition;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "micro.gateway")
public class MicroGatewayProperties {
    private String registryAddress = "nacos://127.0.0.1:8848";
    private String registryGroup = "DEFAULT_GROUP";
    private List<RouterBeanDefinition> routes;

    public String getUrl() {
        String part = registryAddress.substring("nacos".length());
        return new StringBuilder("http")
                .append(part)
                .append("?nacos.group=")
                .append(registryGroup)
                .toString();
    }

    public String getRegistryAddress() {
        return registryAddress;
    }

    public void setRegistryAddress(String registryAddress) {
        this.registryAddress = registryAddress;
    }

    public String getRegistryGroup() {
        return registryGroup;
    }

    public void setRegistryGroup(String registryGroup) {
        this.registryGroup = registryGroup;
    }

    public List<RouterBeanDefinition> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RouterBeanDefinition> routes) {
        this.routes = routes;
    }
}
