package com.ruolin.micro.gateway.server.cache;

import com.google.common.cache.AbstractCache;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.apache.dubbo.config.spring.ReferenceBean;
import org.apache.dubbo.rpc.service.GenericService;

import javax.annotation.CheckForNull;

/**
 * application and Dubbo ReferenceBean mapping
 */

public class RouteReferenceBeanMappingCache extends AbstractCache<String, ReferenceBean<GenericService>> {
    private static final Cache<String, ReferenceBean<GenericService>> BEAN_MAPPING = CacheBuilder.newBuilder().build();

    private RouteReferenceBeanMappingCache() {

    }

    @CheckForNull
    @Override
    public ReferenceBean<GenericService> getIfPresent(Object key) {
        return BEAN_MAPPING.getIfPresent(key);
    }

    public static Cache<String, ReferenceBean<GenericService>> getBeanMapping() {
        return BEAN_MAPPING;
    }
}
