package com.ruolin.micro.gateway.guide.impl;


import com.alibaba.fastjson.JSON;
import com.ruolin.micro.gateway.guide.api.EchoService;
import com.ruolin.micro.gateway.guide.entity.User;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class EchoServiceImpl implements EchoService {
    private volatile AtomicBoolean state = new AtomicBoolean(true);

    @Override
    public String hello(String name) {
        System.out.println("state---------------------------------------"+state.get() );
        return "hello " + name + ",I'm dubbo service provider" + state.get();
    }

    @Override
    public String test1(String name, List<Integer> a) {
        return "hello name=" + name + "a=" + JSON.toJSONString(a);
    }

    @Override
    public String test2(User user, Integer oId) {
        return JSON.toJSONString(user) + ",oId=" + oId;
    }

    @Override
    public String test3(Integer sid, String sn) {
        return "sid=" + sid + ", sn=" + sn;
    }

    @Override
    public String insert(User user) {
        return JSON.toJSONString(user);
    }

    @Override
    public String echo() {
        return "I'm dubbo service";
    }
}
