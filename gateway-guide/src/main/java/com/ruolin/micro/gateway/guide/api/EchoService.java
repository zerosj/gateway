package com.ruolin.micro.gateway.guide.api;



import com.ruolin.micro.gateway.guide.entity.User;

import java.util.List;


public interface EchoService {
    String hello(String name);

    String test1(String name, List<Integer> a);

    String test2(User user, Integer oId);

    String test3(Integer sid, String sn);

    String insert(User user);

    String echo();
}
